package com.okestro.pipeline.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PipelineApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(PipelineApiApplication.class, args);
    }
}
