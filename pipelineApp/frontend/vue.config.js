const path = require("path");
const webpack = require("webpack");
let v_extensions = [".vue", ".js", ".scss", ".json", ".css"];

let v_alias = {
  "@": path.join(__dirname, "src"),
  "@assets": path.join(__dirname, "src/assets"),
  "@font": path.join(__dirname, "src/assets/font"),
  "@image": path.join(__dirname, "src/assets/image"),
  "@css": path.join(__dirname, "src/assets/css"),
  "@js" : path.join(__dirname, "src/assets/js"),
  "@config": path.join(__dirname, "src/config"),
  "@i18n": path.join(__dirname, "src/config/i18n"),
  "@router": path.join(__dirname, "src/config/router"),
  "@store": path.join(__dirname, "src/config/store"),
  "@components": path.join(__dirname, "src/components"),
  "@common": path.join(__dirname, "src/common"),
  "@views": path.join(__dirname, "src/views"),
  "@layout": path.join(__dirname, "src/views/layout"),
  "@page": path.join(__dirname, "src/views/page"),
};

module.exports = {
  publicPath:'/pipeline/app',
  configureWebpack: {
    entry: ["babel-polyfill", "./src/main.js"],
    resolve: {
      extensions: v_extensions,
      alias: v_alias
    },
    plugins: [
      new webpack.ProvidePlugin({
        $: 'jquery',
        jquery: 'jquery',
        'window.jQuery': 'jquery',
        jQuery: 'jquery'
      })
    ]
  },
  lintOnSave : false,
  devServer: {
    host: '0.0.0.0',
    public: 'http://0.0.0.0:8080/pipeline/app',
    proxy: {
      "/api": {
        target: "http://0.0.0.0:8080",
        ws: true,
        changeOrigin: true,
        web: {
          cors: true
        }
      }
    }
  },
  outputDir: "../src/main/resources/static",
  assetsDir: "",
  pluginOptions: {
    i18n: {
      locale: "ko",
      fallbackLocale: "ko",
      localeDir: "locales",
      enableInSFC: false
    }
  }
};
