/*
 * Developed by kbcho@okestro.com on 2020-06-05
 * Last modified 2020-06-05 16:27:17
 */

import Vue from "vue";

// 사용자 권한
Vue.filter("userRole", (code) => {
    switch(code) {
        case 'U12':
            return '프로젝트 관리자';
        case 'U13':
            return '프로젝트 멤버';
        case 'U14':
            return '프로젝트 멤버(CLI)';
    }
});

// 프로젝트 상태
Vue.filter("projectStatus", (code) => {
    switch(code) {
        case 'PS00':
            return '생성 중';
        case 'PS01':
            return '생성';
        case 'PS02':
            return '사용 기간 만료';
        case 'PS03':
            return '삭제 중';
        case 'PS04':
            return '삭제';
        case 'PS05':
            return '변경 중';
    }
});

// 프로젝트 관리자 상태
Vue.filter("projectAdminStatus", (code) => {
    switch(code) {
        case 'PA00':
            return '관리자 없음';
        case 'PA01':
            return '관리자 권한 승인 요청 중';
        case 'PA02':
            return '관리자 있음';
        case 'PA11':
            return '관리자 권한 승인';
        case 'PA12':
            return '관리자 권한 요청 반려';
    }
});

// 프로젝트 신청 상태
Vue.filter("projectApplyStatus", (code) => {
    switch(code) {
        case 'PA01':
            return '신청';
        case 'PA02':
            return '신청';
        case 'PA11':
            return '승인';
        case 'PA21':
            return '승인';
        case 'PA12':
            return '반려';
        case 'PA22':
            return '반려';
        case 'PA13':
            return '취소';
    }
});

// YYYY-MM-DD 형식으로 날짜 반환 - moment 쓸까
Vue.filter('customDate', function (value) {
    let result = '';
    if(value === undefined || isNaN(value) || value === '' || value == null){
        result = '';
    } else {
        if(value.length === 8) {
            result = value.substring(0,4) + '-' + value.substring(4,6) + '-' + value.substring(6,8);
        } else {
            let customDate = new Date(value);

            var year = customDate.getFullYear();        //yyyy
            var month = (1 + customDate.getMonth());    //M
            month = month >= 10 ? month : '0' + month;  //month 두자리로 저장
            var day = customDate.getDate();             //d
            day = day >= 10 ? day : '0' + day;          //day 두자리로 저장
            result = year + '-' + month + '-' + day;     //'-' 추가하여 yyyy-mm-dd 형태 생성 가능
        }
    }

    return result;
    // return moment(value).format("YYYY-MM-DD");
});

Vue.filter('phoneNumber', function(num) {
    let formatNum = '';
    let type = 1;

    if(num.length === 11){
        if(type === 0){
            formatNum = num.replace(/(\d{3})(\d{4})(\d{4})/, '$1-****-$3');
        }else{
            formatNum = num.replace(/(\d{3})(\d{4})(\d{4})/, '$1-$2-$3');
        }
    }else if(num.length === 8){
        formatNum = num.replace(/(\d{4})(\d{4})/, '$1-$2');
    }else{
        if(num.indexOf('02') === 0){
            if(type === 0){
                formatNum = num.replace(/(\d{2})(\d{4})(\d{4})/, '$1-****-$3');
            }else{
                formatNum = num.replace(/(\d{2})(\d{4})(\d{4})/, '$1-$2-$3');
            }
        }else{
            if(type === 0){
                formatNum = num.replace(/(\d{3})(\d{3})(\d{4})/, '$1-***-$3');
            }else{
                formatNum = num.replace(/(\d{3})(\d{3})(\d{4})/, '$1-$2-$3');
            }
        }
    }
    return formatNum;
})

Vue.filter('targetSection', function(type) {
    let alarmType = '플랫폼';

    if(type === 'org') {
        alarmType = '입주기관';
    } else if(type === 'project') {
        alarmType = '프로젝트';
    }

    return alarmType;
})

Vue.filter('alarmTarget', function(type) {
    let alarmTarget = '플랫폼 관리자';

    if(type === 'org') {
        alarmTarget = '입주기관 관리자';
    } else if(type === 'project') {
        alarmTarget = '프로젝트 멤버';
    }

    return alarmTarget;
})

// 알람 유형 구분
Vue.filter("alarmType", (code) => {
    switch(code) {
        case 'ART00':
            return '임계치 알람';
        case 'ART01':
            return '상태 알람';
        case 'ART02':
            return '특정 문자열 알람';
    }
});

// 알람 상태 구분
Vue.filter("alarmStatusCode", (code) => {
    switch(code) {
        case 'AST00':
            return 'warning';
        case 'AST01':
            return 'critical';
    }
});

// 알람 읽음 상태
Vue.filter("alarmReadAt", (code) => {
    switch(code) {
        case 'Y':
            return '읽음';
        case 'N':
            return '읽지 않음';
    }
});

// 사용자 권한
Vue.filter("mktSvcType", (code) => {
    switch(code) {
        case 'instl':
            return '설치형';
        case 'API':
            return 'API형';
        case 'share':
            return '공유형';
        default:
            return '-';
    }
});

Vue.mixin({
    methods: {
        checkAlphaNum(value) {
            let alphaNumPattern = new RegExp('^[a-z0-9-_\s]+$');
            return !alphaNumPattern.test(value);
        }
    }
});