export default {
    install(Vue) {
        Vue.prototype.$unixToDate = function(unixTimeStamp) {
            if (unixTimeStamp !== null && unixTimeStamp !== undefined && unixTimeStamp !== '') {
                let milliseconds = unixTimeStamp;
                let dateObject = new Date(milliseconds * 1000);
                let year = dateObject.getFullYear();
                let month = dateObject.getMonth() + 1;
                let day = dateObject.getDate();

                if (day < 10) {
                    day = '0' + day;
                }
                if (month < 10) {
                    month = '0' + month;
                }
                return year + '-' + month + '-' + day;
            }

            return '';
        };

        Vue.prototype.$dateTimeToUnix = function(dateTime) {
            if (dateTime !== null && dateTime !== undefined && dateTime !== '') {
                return Math.floor(new Date(dateTime).getTime() / 1000);
            }

            return '';
        };

        Vue.prototype.$unixToDateTime = function(unixTimeStamp) {
            if (unixTimeStamp !== null && unixTimeStamp !== undefined && unixTimeStamp !== '') {
                let milliseconds = unixTimeStamp;
                let dateObject = new Date(milliseconds * 1000);
                let year = dateObject.getFullYear();
                let month = dateObject.getMonth() + 1;
                let day = dateObject.getDate();
                let hour = '0' + dateObject.getHours();
                let min = '0' + dateObject.getMinutes();
                let sec = '0' + dateObject.getSeconds();

                if (day < 10) {
                    day = '0' + day;
                }
                if (month < 10) {
                    month = '0' + month;
                }

                // return year + '-' + month + '-' + day + ' ' + hour.substr(-2) + ':' + min.substr(-2) + ':' +  sec.substr(-2);
                return year + '-' + month + '-' + day + ' ' + hour.substr(-2) + ':' + min.substr(-2);
            }

            return '';
        };
    }
}