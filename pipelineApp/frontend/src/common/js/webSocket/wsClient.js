// import './lib/sockjs.js';
// import './lib/stomp.js';

import Vue from "vue";
Vue.prototype.EventBus = new Vue();
// var EventBus = new Vue();

// websocket vue
import SockJS from "sockjs-client";
import Stomp from "webstomp-client";

new Vue({
  name: "websocketdemo",
  data() {
    return {
      received_messages: [],
      send_message: null,
      connected: false
    };
  },
  methods: {
    send() {
      console.log("Send message:" + this.send_message);
      if (this.stompClient && this.stompClient.connected) {
        const msg = { name: this.send_message };
        console.log(JSON.stringify(msg));
        // this.stompClient.send("/app/hello", JSON.stringify(msg), {});
      }
    },
    connect() {
      this.socket = new SockJS(process.env.VUE_APP_ADMIN_APP_URL+"/websocket/egovadmin",{withCredentials: true});
      this.stompClient = Stomp.over(this.socket);
      console.log("socketInfo", this.socket);
      this.stompClient.connect(
        {},
        frame => {
          this.connected = true;
          console.log(frame);
          this.stompClient.subscribe("/topic/onMessage", tick => {
            console.log(tick);
            this.EventBus.$emit("ddd", tick.body);
            // this.received_messages.push(JSON.parse(tick.body).content);
          });
        },
        error => {
          console.log(error);
          this.connected = false;
        }
      );
    },
    disconnect() {
      if (this.stompClient) {
        this.stompClient.disconnect();
      }
      this.connected = false;
    },
    tickleConnection() {
      this.connected ? this.disconnect() : this.connect();
    }
  },
  created() {
      console.log("ws created");
    this.connect();
  }
});

