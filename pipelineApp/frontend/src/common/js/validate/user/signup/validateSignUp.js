import { extend } from 'vee-validate'
import { required, length, alpha_num, confirmed, email} from 'vee-validate/dist/rules';

/* 공통 - 아이디 필수 */
extend('required', {
    ...required,
    message: ${field}+ '입력이 필요합니다..',
    computesRequired:true
});

extend('required_id', {
    ...required,
    message: '아이디 입력이 필요합니다..',
    computesRequired:true
});

/* 공통 - 아이디 규칙 */
extend('alpah_num_id', {
    ...alpha_num,
    message: '영문과 숫자만 사용 가능합니다.'
});

/* 공통 - 아이디 길이  */
extend('length_id', {
    ...length,
    validate(value, {min, max}) {
       return value.length >= min && value.length <= max;
    },
    params: ["min", "max"],
    message: self.min + '~' + self.max + '자 사이의 아이디만 가능합니다.'
});

/* 공통 - 비밀번호 입력 */
extend('required_pw', {
    ...required,
    message: '비밀번호 입력이 필요합니다.',
    computesRequired:true
});

/* 공통 - 비밀번호 확인 */
extend('confirmedBy',{
    ...confirmed,
    params: ['target'],
    validate(value, { target }) {
        return value === target;
    },
    message: '비밀번호가 일치 하지 않습니다.'
})

/* 공통 -비밀번호 규칙 */
extend('rules_pw', {
    message: '영문, 숫자, 특수 문자(!@#$%^*+=-) 중 세가지를 혼용하여 입력 가능합니다.(9~15자)',
    validate: value => {
        return /^(?=.*[a-zA-Z])(?=.*[!@#$%^*+=-])(?=.*[0-9]).{9,15}$/.test(value)
    }
})

/* 회원가입 - 이름*/
extend('required_name', {
    ...required,
    message: '이름을 입력해주세요.',
    computesRequired:true
});

extend('onlyKorean',{
    message: '한글만 입력 가능합니다.',
    validate: value => {
        return /^[가-힣]+$/.test(value)
    }
})

extend('length_name', {
    ...length,
    validate(value, {min, max}) {
        return value.length >= min && value.length <= max;
    },
    params: ["min", "max"],
    message: '2 ~ 5자 사이의 이름만 가능합니다.'
});

/* 회원가입 - 입주기관*/


extend('required_orgNm', {
    ...required,
    message: '입주기관 선택이 필요합니다.',
    computesRequired:true
});

/* 회원가입 - 업체정보*/
extend('required_entrpsNm', {
    ...required,
    message: '업체명 입력이 필요합니다.',
    computesRequired:true
});

/* 아이디 찾기, 비밀번호 찾기 - 이메일 앞 부분 */
extend('required_email', {
    ...required,
    message: '이메일을 입력해주세요.',
    computesRequired:true
});

extend('required_emailAddress', {
    ...required,
    message: '이메일 주소를 입력해주세요.',
    computesRequired:true
});

extend('email_front', {
    message: '이메일 형식이 잘못되었습니다.',
    validate: value => {
        return /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])+$/i.test(value)
    }
});

extend('email_back', {
    message: '이메일 주소 형식이 잘못되었습니다.',
    validate: value => {
        return /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i.test(value)
    }
});

extend('required_brthdy', {
    ...length,
    message: '생년월일을 선택해주세요.',
    validate(value, {min}) {
        return value.length >= min;
    },
    params: ["min"],
    computesRequired:true
});