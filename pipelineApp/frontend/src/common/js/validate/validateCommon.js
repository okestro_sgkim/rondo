import { extend } from 'vee-validate'
import { required, length, alpha_num, confirmed, email,digits} from 'vee-validate/dist/rules';

/* 공통 - 아이디 필수 */

extend('required_id', {
    ...required,
    message: '아이디 입력이 필요합니다.',
    computesRequired:true
});
/* 공통 - 선택 필수 */
extend('required_select', {
    ...required,
    message: '선택이 필요합니다.',
    computesRequired:true
});

/* 공통 - 아이디 규칙 */
extend('alpah_num_id', {
    ...alpha_num,
    message: '영문과 숫자만 사용 가능합니다.'
});

/* 공통 - 아이디 길이  */
extend('length_id', {
    ...length,
    validate(value, {min, max}) {
        return value.length >= min && value.length <= max;
    },
    params: ["min", "max"],
    message: `{min} ~ {max}자 사이의 아이디만 가능합니다.`
});
/* 공통 - 최대 */
extend('digits', {
    ...digits,
    validate(value, {num}) {
        return value.length == num;
    },
    params: ['num'],
    message: '{num}자를 사용해야합니다.'
});

/* 공통 - 비밀번호 입력 */
extend('required_pw', {
    ...required,
    message: '비밀번호 입력이 필요합니다.',
    computesRequired:true
});

/* 공통 - 비밀번호 확인 */
extend('confirmedBy',{
    ...confirmed,
    params: ['target'],
    validate(value, { target }) {
        return value === target;
    },
    message: '비밀번호가 일치 하지 않습니다.'
})

/* 공통 -비밀번호 규칙 */
extend('rules_pw', {
    message: '영문, 숫자, 특수 문자(!@#$%^*+=-) 중 세가지를 혼용하여 입력 가능합니다.(9~15자)',
    validate: value => {
        return /^(?=.*[a-zA-Z])(?=.*[!@#$%^*+=-])(?=.*[0-9]).{9,15}$/.test(value)
    }
})

/* 회원가입 - 이름*/
extend('required_name', {
    ...required,
    message: '이름을 입력해주세요.',
    computesRequired:true
});

extend('onlyKorean',{
    message: '한글만 입력 가능합니다.',
    validate: value => {
        return /^[가-힣]+$/.test(value)
    }
})

extend('length_name', {
    ...length,
    validate(value, {min, max}) {
        return value.length >= min && value.length <= max;
    },
    params: ["min", "max"],
    message: '2 ~ 5자 사이의 이름만 가능합니다.'
});

/* 회원가입 - 입주기관*/


extend('required_orgNm', {
    ...required,
    message: '입주기관 선택이 필요합니다.',
    computesRequired:true
});

/* 회원가입 - 업체정보*/
extend('required_entrpsNm', {
    ...required,
    message: '업체명 입력이 필요합니다.',
    computesRequired:true
});

/* 아이디 찾기, 비밀번호 찾기 - 이메일 앞 부분 */
extend('required_email', {
    ...required,
    message: '이메일을 입력해주세요.',
    computesRequired:true
});

extend('required_emailAddress', {
    ...required,
    message: '이메일 주소를 입력해주세요.',
    computesRequired:true
});

extend('email_front', {
    message: '이메일 형식이 잘못되었습니다.',
    validate: value => {
        return /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])+$/i.test(value)
    }
});

extend('email_back', {
    message: '이메일 주소 형식이 잘못되었습니다.',
    validate: value => {
        return /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i.test(value)
    }
});

/* 이메일 전체*/
extend('email_full',{
    message: '이메일 형식이 잘못되었습니다.',
    validate: value => {
        return /^\w+([\.-]?\w+)+@\w+([\.:]?\w+)+(\.[a-zA-Z0-9]{2,3})+$/i.test(value)
    }
});

extend('required_brthdy', {
    ...length,
    message: '생년월일을 선택해주세요.',
    validate(value, {min}) {
        return value.length >= min;
    },
    params: ["min"],
    computesRequired:true
});

/* 공통 - 필수*/
extend('required', {
    ...required,
    message: '{name}을(를) 입력해주세요.',
    params: ["name"],
    computesRequired:true
});

/* 공통 - 필수*/
extend('required-select', {
    ...required,
    message: '{name}을(를) 선택해주세요.',
    params: ["name"],
    computesRequired:true
});

/* 공통 - 전화번호 */
extend('rules-telno', {
    message: ' 숫자, 특수 문자(-)를 혼용하여 입력 해야됩니다. 예시)02-222-222',
    validate: value => {
        //return /^(?=.*[a-zA-Z])(?=.*[!@#$%^*+=-])(?=.*[0-9]).{9,15}$/.test(value)
        //return /^(?=.*[-])(?=.*[0-9])$.test(value)
        //[-!#$%& amp;'*+./0-9=?A-Z^_a-z{|}~]
        //return /^([0-9]|[-])+$/.test(value)
        return /^(?=.*[-])(?=.*[0-9]).{0,}$/.test(value)

    }
})

/* 공통 - 숫자 */
extend('rules-number', {
    message: ' 숫자만 입력 가능합니다.',
    validate: value => {
        return /^(?=.*[0-9]).{0,}$/.test(value)
    }
})

/* 공통 - 길이  */
extend('length_common', {
    ...length,
    validate(value, {min, max}) {
        return value.length >= min && value.length <= max;
    },
    params: ['min', 'max'],
    message: `{min} ~ {max}자 사이 길이만 사용 가능합니다.`
});

/* 공통 영문,한글,숫자 규칙 */
extend('alpah_korean_num', {
    message: '영문, 한글, 숫자만 사용 가능합니다.',
    validate: value => {
        return /^[가-힣a-zA-Z0-9-_\s]+$/.test(value)
    }
});

/* 공통 URL 규칙 */
extend('url', {
    message: 'URL 형식이 올바르지 않습니다.',
    validate: value => {
        return /(http(s)?:\/\/)([a-z0-9\w]+\.*)+[a-z0-9]{2,4}/gi.test(value)
    }
});

/* 공통 URL 규칙 */
extend('short_url', {
    message: 'URL 형식이 올바르지 않습니다.',
    validate: value => {
        return /(\/)([a-z0-9\w]+\.*)+[a-z0-9]{2,4}/gi.test(value)
    }
});

/* 서비스 명 */
extend('service_nm', {
    ...required,
    message: '서비스 명을 입력해주세요.',
    computesRequired:true
});

/* 서비스 명 길이 */
extend('length_service_nm', {
    ...length,
    validate(value, {min, max}) {
        return value.length >= min && value.length <= max;
    },
    params: ['min', 'max'],
    message: '서비스명은 {min} ~ {max} 자 사이로 입력해야 합니다.'
});

/* 공통 영문(소문자),숫자 규칙 */
extend('alpah_num', {
    message: '영문(소문자), 숫자만 사용 가능합니다.',
    validate: value => {
        return /^[a-z0-9-_\s]+$/.test(value)
    }
});

/* 공통 URL 영문(대소문자),숫자 규칙 */
extend('url_alpah_num', {
    message: '영문(대/소문자), 숫자만 사용 가능합니다.',
    validate: value => {
        return /^[a-zA-Z0-9/.]+$/.test(value)
    }
});

extend('withoutSpace', {
    message: '공백은 허용되지 않습니다.',
    validate: value => {
        return !/\s/.test(value)
    }
});

extend('broker_id', {
    message: 'admin, administrator, root, user는 사용 할수 없습니다.',
    validate: value => {
        if (value === 'admin' || value === 'administrator' || value === 'root' || value === 'user') {
            return false;
        } else {
            return true;
        }
    }
});

/* 공통 모음, 자음, 한글, 영어, 특수문자 규칙 */
extend('vowels_consonant', {
    message: '형식이 올바르지 않습니다.',
    validate: value => {
        return !/([^가-힣0-9a-zA-Z\s<>\(\)\[\]\"\'.,/&%:!=@#$?*_+-])/.test(value)
    }
});

/* 사업자 등록번호  */
extend('bizmanAddNo', {
    message: '올바른 사업자등록번호를 입력하세요.',
    validate: value => {
      return  /^\d{3}-\d{2}-\d{5}$/.test(value)
    }
});