import 'es6-promise';
import Vue from "vue";
import App from "./App";
import router from "@config/router/router";
import store from "@config/store/index";
import i18n from "@config/i18n";
import '@css/bootstrap.min.css';
import '@css/style.css';
import '@css/res.css';
import '@css/animate.css';
import '@css/vue-select.css'
import '@assets/font-awesome/css/font-awesome.css'
import Notifications from 'vue-notification';
import components from "./components";
import VueCookie from 'vue-cookie';
import VueSelect from 'vue-select';
import {ValidationObserver, ValidationProvider} from 'vee-validate';
import './common/js/validate/validateCommon';

import VueMoment from 'vue-moment';
import "@common/js/filters/filters"
import CKEditor from 'ckeditor4-vue';

import LoadScript from 'vue-plugin-load-script';

Vue.use(LoadScript);

//전역컴포넌트화
Vue.use(components);
Vue.component('ValidationProvider', ValidationProvider);
Vue.component('ValidationObserver', ValidationObserver);
Vue.component('v-select', VueSelect);
Vue.use(VueCookie);

Vue.use(VueMoment);
Vue.use(Notifications);

Vue.use(CKEditor);
Vue.config.productionTip = false;
Vue.prototype.$EventBus = new Vue();
Vue.prototype.$pageSize = 5; // 목록 하단에 노출되는 페이지번호 개수
Vue.prototype.$pageDataSize = 10; // 목록 행 개수



new Vue({
  ValidationProvider,
  ValidationObserver,
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount("#app");
