export default {
    // ------ State bypass ------
    alterInfo(state) {
        return state.info;
    },
    nextPath(state) {
        return state.nextPath;
    },
    getLoding(state) {
        return state.loading;
    }

}