export default {

    setAlterInfo(state, param) {
        state.info = param;
    },
    setNextPath(state, param) {
        state.nextPath = param;
    },
    spinnerOn(state, param) {
        state.spinner = param;
    }

}