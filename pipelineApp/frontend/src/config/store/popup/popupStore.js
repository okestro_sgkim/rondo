import state     from './popupState';
import mutations from './popupMutaions';
import actions   from './popupActions';
import getters   from './popupGetters';

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}