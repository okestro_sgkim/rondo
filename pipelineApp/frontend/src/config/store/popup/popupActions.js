export default {
    setAlterInfo(context, param) {
        context.commit('setAlterInfo', param);
    }
    ,
    setNextPath(context, param) {
        context.commit('setNextPath', param);
    },
    spinnerOn(context, param) {
        context.commit('spinnerOn', param);
    },

}