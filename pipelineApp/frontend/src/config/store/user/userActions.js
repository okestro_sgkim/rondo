export default {

	setUser(context, user) {

		context.commit('setUser', user);
	},

	setUserRole(context, role) {

		context.commit('setUserRole', role);
	},

	// ===== [STR] (insert) store action 생성 ## stored access path list (2021.01.20 by.invangs)
	initAccessPathList(context) {

		context.commit('initAccessPathList');
	},

	setAccessPathList(context, list) {

		context.commit('setAccessPathList', list);
	},

	pushAccessPathList(context, list) {

		context.commit('pushAccessPathList', list);
	}
	// ===== [END] (insert) store action 생성 ## stored access path list (2021.01.20 by.invangs)
}
