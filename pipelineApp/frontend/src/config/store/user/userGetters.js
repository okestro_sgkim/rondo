export default {

	info(state) {

		return state.info;
	},

	Id(state) {

		return state.info.id;
	},

	role(state) {

		return state.info.role;
	},

	// ===== [STR] (insert) store getter 생성 ## stored access path list (2021.01.20 by.invangs)
	accessPathList(state) {

		return state.accessPathList;
	}
	// ===== [END] (insert) store getter 생성 ## stored access path list (2021.01.20 by.invangs)
}
