export default {

    info : {

        userNo : '',
        userId : '',
        ssoKey : '',
        userNm : '',
        userSeCd : '',
        deptCd : '',
        deptNm : '',
        orgCd : '',
        orgNm : '',
        roleId : '',
        errCode : '',
        errMsg : '',
        uaaAccessToken : '',
        uaaIdToken : '',
        uaaSsoKey : '',
        uaaStatusCode : '',
        uaaStatusMsg : '',
        uaaRedirectUri : '',
        uaaAt : false,
        keycloakAuthorizeState : '',
        keycloakAuthorizeCode : '',
        keycloakAccessToken : '',
        keycloakStatusCode : '',
        keycloakTokenUrl : '',
        keycloakLogoutUrl : '',
        keycloakAt : false,
        prjctId : '',
        prjctNm : '',
        authIs : false,
        role : {},
        unreadAlarmList : []
    },

    // (update) state.info.accessPathList 에서 state.accessPathList 으로 이동 ## stored access path list (2021.01.20 by.invangs)
    accessPathList : []
}