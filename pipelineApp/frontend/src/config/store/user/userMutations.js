export default {

	setUser(state, user) {

		state.info = user;
	},

	setUserRole(state, role) {

		state.info.role = role;
	},

	// ===== [STR] (insert) store mutations 생성 ## stored access path list (2021.01.20 by.invangs)
	initAccessPathList(state) {

		state.accessPathList = [];
	},

	setAccessPathList(state, list) {

		state.accessPathList = list;
	},

	pushAccessPathList(state, list) {

		state.accessPathList.push(...list);
	}
	// ===== [END] (insert) store mutations ## stored access path list (2021.01.20 by.invangs)
}
