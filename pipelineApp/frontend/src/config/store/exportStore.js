import Vue from 'vue';
import Vuex from 'vuex';
// import { mapGetters, mapMutations, mapState } from 'vuex';

/* Global store */
import userStore from "./user/userStore";
import popupStore from "./popup/popupStore";
import createPersistedState from "vuex-persistedstate";
// import SecureLS from "secure-ls";

Vue.use(Vuex);


// const ls = new SecureLS({ isCompression: false });

let store = new Vuex.Store({

	state : {

	},

	mutations : {

	},

	actions : {

	},

	getters : {

	},

	modules : {
		user : userStore,
		popup : popupStore
	},

    plugins : [
        createPersistedState(
            // {
            //     storage: {
            //         getItem: key => ls.get(key),
            //         setItem: (key, value) => ls.set(key, value),
            //         removeItem: key => ls.remove(key)
            //     }
            // }
        )
    ],
});


export default store;
