import Vue from "vue";
import VueRouter from "vue-router";


import MainLayOut from "../../views/layout/main/MainLayOut";
import sample1 from "../../views/page/sample1";
import sample2 from "../../views/page/sample2";

Vue.use(VueRouter);


const routes = [
	{
		path: "/", component: MainLayOut, children: [
			{path : "", redirect : "/sample1"},
			{path : "/sample1/", name : "sample1", component : sample1},
			{path : "/sample2/", name : "sample2", component : sample2},
		]
	},

];


const router = new VueRouter({

	mode : "history",
	base : process.env.BASE_URL,
	routes,

	scrollBehavior(to, from, savedPosition) {
		if (savedPosition) {
			return savedPosition;
		}
		if (to.hash) {
			return {selector : to.hash};
		}
		return {x : 0, y : 0};
	}
});


export default router;
