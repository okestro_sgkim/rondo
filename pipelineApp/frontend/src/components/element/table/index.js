import basicTable from "./basicTable";
import dataTable from "./dataTable";
import testTable from "./testTable";
export default {
  install(Vue) {
    Vue.component(basicTable.name, basicTable);
    Vue.component(dataTable.name, dataTable);
    Vue.component(testTable.name, testTable);
  }
};
