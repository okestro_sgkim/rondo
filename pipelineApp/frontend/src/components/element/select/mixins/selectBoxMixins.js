export const selectBoxMixins = {
    data() {
        return {
            isActive : false
            , hide : true
            , viewName : "전체"
            , selectItem : {
                name : "전체"
                ,value : "all"
            },
            disabledStyle: {
                backgroundColor: "#eee",
                cursor: 'not-allowed'
            }
        }
    },
    methods: {
        setHide : function (at, value) {
            this.viewName = value.name;
            this.isActive = at;
            this.hide = true;
            this.selectItem = value;
            this.callback();
        }
        ,outside: function() {
            this.isActive = false;
            this.hide = true;
        }
        ,callback : function () {
            this.$emit('setSelected',this.selectItem,this.index);
        }
    },
    directives: {
        'click-outside': {
            bind: function(el, binding, vNode) {
                // Provided expression must evaluate to a function.
                if (typeof binding.value !== 'function') {
                    const compName = vNode.context.name
                    let warn = `[Vue-click-outside:] provided expression '${binding.expression}' is not a function, but has to be`
                    if (compName) { warn += `Found in component '${compName}'` }
                    console.warn(warn)
                }
                // Define Handler and cache it on the element
                const bubble = binding.modifiers.bubble
                const handler = (e) => {
                    if (bubble || (!el.contains(e.target) && el !== e.target)) {
                        binding.value(e)
                    }
                }
                el.__vueClickOutside__ = handler

                // add Event Listeners
                document.addEventListener('click', handler)
            },

            unbind: function(el, binding) {
                // Remove Event Listeners
                document.removeEventListener('click', el.__vueClickOutside__)
                el.__vueClickOutside__ = null

            }
        }
    }

}