import * as components from "./exportComponents";
export default function install(Vue) {
  Object.values(components).forEach(function(component) {
    Vue.use(component);
  });
}
