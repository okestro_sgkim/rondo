
$(document).ready(function(){

  $(".navbar-collapse a").on('click', function() {
    $(".navbar-collapse.collapse").removeClass('in');
  });

  // Navigation scrolls
  $('.navbar-nav li a').bind('click', function(event) {
    $('.navbar-nav li a').removeClass('active');
    $(this).closest('li a').addClass('active');
    var $anchor = $(this);
    var nav = $($anchor.attr('href'));
    if (nav.length) {
      $('html, body').stop().animate({
        scrollTop: $($anchor.attr('href')).offset().top
      }, 1000, 'easeInOutExpo');

      event.preventDefault();
    }
  });


  //jQuery to collapse the navbar on scroll
  $(window).scroll(function() {
    if ($(".navbar-default").offset().top > 50) {
      $(".navbar-fixed-top").addClass("top-nav-collapse");
      $('.goToTop').css({'opacity':.7})
    } else {
      $(".navbar-fixed-top").removeClass("top-nav-collapse");
      $('.goToTop').css({'opacity':0})
    }

  });

  $(".goToTop").click(function(event) {
      event.preventDefault();
      $('html,body').animate({ scrollTop: $(this.hash).offset().top - 100 }, 500);
  });

  
})